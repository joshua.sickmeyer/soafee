# SOAFEE Hugo Website

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**
- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Custom Domain Setup](#custom-domain-setup)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add/Modify content within the `/content` directory
    - All content files are in markdown `md` format
1. (Optional) Generate the website files for manual deployment by running the `hugo` command. Then, place the generated content from within the `/public` directory to the server of your choice.

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/soafee/`.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/soafee/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain](#custom-domain-setup): `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Custom Domain Setup
### Features
- Besides including your own domain, you can add your custom subdomain to your GitLab Pages project (e.g., subdomain.example.com)
- You can enter more than one domain alias per project (e.g., example.com, example.net my.example.org and another-example.com pointing to your project under mynamespace.gitlab.io or mynamespace.gitlab.io/myproject). A domain alias is like having multiple front doors to one location.
- If you want to enable an HTTPS secure connection to your domains, you can affix your own SSL/TLS digital certificate to each custom domain or subdomain you've added to your projects.

### Steps to set up a custom domain in GitLab
- From your project's dashboard, go to **Settings () > Pages > New Domain**
- Add your domain to the first field: `mydomain.com`
- If you have an SSL/TLS digital certificate and its key, add them to their respective fields. If you don't, just leave the fields blank.
- Click on Create New Domain.
- Finally, access your domain control panel and create a new DNS A record pointing to the IP of your GitLab Pages server:

> mydomain.com A ##.##.##.##

*More information: https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains*

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
